#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import sys
sys.path.append("venv/Lib/site-packages/")
import Configuration.default_config as config

import multiprocessing as mp
import re
import os
import json
from datetime import datetime, timedelta
import smtplib
import email
import email.mime.application
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


NUMBER_OF_CORES = mp.cpu_count()
RESULT_FILENAME = 'Result Model.csv'
FILENAME_TEMPLATE = config.dir_log + '/{date}-SHWDB.log'
log_pattern = re.compile(r"^INFORMATION.*(?<=error:')(.*)(?=').*(?<=zabbix:)(.*)?$")
manager = mp.Manager()
result = manager.list()

def mail_send(configuration_serv, person, file):
    msg = MIMEMultipart()
    subject = 'Report Model from ' + config.segment
    msg['Subject'] = subject
    msg['From'] = configuration_serv["mail_from"]
    body = MIMEText("""
                    This email is generated by an automated system.
                    Please do not reply.
                    """)
    msg.attach(body)
    mail_to = mails['mails']['Persons'][person]['Mail']
    filename = file
    fp = open(filename, 'rb')
    att = email.mime.application.MIMEApplication(fp.read(), _subtype="zip")
    fp.close()
    att.add_header('Content-Disposition', 'attachment', filename=filename)
    msg.attach(att)
    server = smtplib.SMTP(configuration_serv["server"], port=configuration_serv["port"])
    server.ehlo(configuration_serv["ehlo"])
    server.sendmail(msg['From'], mail_to, msg.as_string())
    server.quit()

def write_result_csv(result):
    work_f = open(RESULT_FILENAME, "w")
    work_f.write('="Model";="Server"\n')
    for line in result:
        result = map(lambda x: f'="{x}"', line)
        work_f.write(';'.join(result)+"\n")
    work_f.close()

def get_list_with_unique_tuples(tuples):
    result = list()
    for tuple in tuples:
        if tuple not in result:
            result.append(tuple)
    return result

def get_logfile_name():
    yesterday_str = datetime.strftime(datetime.now() - timedelta(1), '%Y%m%d')
    name = FILENAME_TEMPLATE.format(date=yesterday_str)
    return name

def process(line):
    match = log_pattern.match(line)
    if match:
        model, server = match.groups()
        if model:
            res_tuple = (model, server)
            result.append(res_tuple)

def worker(filename, chunk_start, chunk_size):
    with open(filename, 'r') as f:
        f.seek(chunk_start)
        lines = f.read(chunk_size).splitlines()
        for line in lines:
            process(line)

def chunkify(fname, size=1024):
    file_end = os.path.getsize(fname)
    with open(fname, 'rb') as f:
        chunk_end = f.tell()
        while True:
            chunk_start = chunk_end
            f.seek(size, 1)
            f.readline()
            chunk_end = f.tell()
            yield chunk_start, chunk_end - chunk_start
            if chunk_end > file_end:
                break


pool = mp.Pool(NUMBER_OF_CORES)

jobs = []
filename = get_logfile_name()
for chunk_start, chunk_size in chunkify(filename):
    jobs.append(pool.apply_async(worker, (filename, chunk_start, chunk_size)))

for job in jobs:
    job.get()

pool.close()
pool.join()

result = get_list_with_unique_tuples(result)
write_result_csv(result)

path_conf_mail = config.path_dir + 'Mails.json'
with open(path_conf_mail, 'r') as mail:
    mails = json.load(mail)
    for mail in mails['mails']['Persons']:
        mail_send(mails['mails']['Configuration'][config.segment], mail, RESULT_FILENAME)
