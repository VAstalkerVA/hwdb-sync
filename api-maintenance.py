#!/usr/bin/python3


import sys
sys.path.append("venv/Lib/site-packages/")
import Configuration.default_config as config

import json
import fcntl
import os
import pymysql
import requests
from multiprocessing.pool import ThreadPool
from subprocess import Popen
from subprocess import PIPE
from datetime import datetime


def connect_maria(configure):
    maintenance = configure['SQL_maintenance_get']
    connection_hardwaredb = pymysql.connect(host=configure['server'], user=configure['user'], password=configure['password'], database=configure['db'])
    cursor = connection_hardwaredb.cursor()
    cursor.execute(maintenance)
    result = cursor.fetchall()
    lists_item_db = []
    for rows in result:
        f = []
        for row in rows:
            f.append(row)
        lists_item_db.append(f)
    connection_hardwaredb.close()
    return lists_item_db

def loging_file(severity, message):
    date = datetime.now().strftime("%Y%m%d")
    time = datetime.now().strftime("%H:%M")
    logfile_name = str(date) + "-SHWDB.log"
    logfile = config.dir_log + "/" + logfile_name
    record = severity + "|" + str(time) + "|" + message + "\n"
    write_log = open(logfile, "a")
    write_log.write(record)
    write_log.close()

def connect_pgsql(config_pgsql):
    walkstring = (f"PGPASSWORD={config_pgsql['dbpassword']} psql -h {config_pgsql['dbhost']} -p "
                  f"{config_pgsql['dbport']} -U {config_pgsql['dbuser']} -w -d {config_pgsql['dbread']} " 
                  f"-c \"(select row_to_json(v.*) from ({config.sql_maintenance_zbx}) as v)\"")
    child = Popen(walkstring, shell=True, stdout=PIPE)
    result = child.stdout.readlines()
    list_maintenance = []
    for x in result:
        x = x.decode("utf-8")
        x = x.replace("\n", "")
        list_maintenance.append(x)
    item = [0, 0, -1, -1]
    for x in item:
        list_maintenance.pop(x)
    list_maintenance_zbbx = {}
    for item in list_maintenance:
        item = json.loads(item)
        if item['maintenanceid'] not in list_maintenance_zbbx:
            list_maintenance_zbbx[item['maintenanceid']] = {}
            list_maintenance_zbbx[item['maintenanceid']]["maintenanceid"] = item['maintenanceid']
            list_maintenance_zbbx[item['maintenanceid']]["maintenancename"] = item["maintenancename"]
            list_maintenance_zbbx[item['maintenanceid']]["description"] = item["description"]
            list_maintenance_zbbx[item['maintenanceid']]["active_since"] = item["active_since"]
            list_maintenance_zbbx[item['maintenanceid']]["active_till"] = item["active_till"]
            list_maintenance_zbbx[item['maintenanceid']]["hostid"] = item["hostid"]
            list_maintenance_zbbx[item['maintenanceid']]["hostname"] = item["hostname"]
            list_maintenance_zbbx[item['maintenanceid']]["status"] = item["status"]
            list_maintenance_zbbx[item['maintenanceid']]["visiblename"] = item["visiblename"]
            list_maintenance_zbbx[item['maintenanceid']]["groupname"] = item["groupname"]
            list_maintenance_zbbx[item['maintenanceid']]["groupid"] = item["groupid"]
    return list_maintenance_zbbx

def sorted_hosts(maintenance_hwdb, maintenance_zabbix, zabbix):
    deleted = []
    update = []
    add = []
    if maintenance_hwdb == None:
        delet = []
        for item in maintenance_zabbix:
            if maintenance_zabbix[item]['active_till'] > int(datetime.now().timestamp()):
                continue
            else:
                if (int(datetime.now().timestamp()) - maintenance_zabbix[item]['active_till']) > differentday:
                    delet.append(str(maintenance_zabbix[item]['maintenanceid']))
                else:
                    continue
        deleted.append({"zbx_server":zabbix_servers[config.segment][zabbix]['selfportal_name'], "maintenanceid":delet})
    else:
        delet = []
        list_hst_hwdb = []
        for i in maintenance_hwdb:
            list_hst_hwdb.append(i)
        for item in maintenance_zabbix:
            if maintenance_zabbix[item]['visiblename'] in maintenance_hwdb and maintenance_zabbix[item]['active_till'] > int(datetime.now().timestamp()):
                diction = {
                    "zbx_server": zabbix_servers[config.segment][zabbix]['selfportal_name'],
                    "maintenanceid": str(maintenance_zabbix[item]['maintenanceid']),
                    "hostname": [maintenance_zabbix[item]['hostname']],
                    "start_date": maintenance_hwdb[maintenance_zabbix[item]['visiblename']]['startdate'],
                    "period": maintenance_hwdb[maintenance_zabbix[item]['visiblename']]['duration']
                }
                if maintenance_hwdb[maintenance_zabbix[item]['visiblename']]['interfaces'][0] != None:
                    for i in maintenance_hwdb[maintenance_zabbix[item]['visiblename']]['interfaces']:
                        if 'tags' not in diction:
                            diction['tags'] = []
                        diction['tags'].append({"tag": "interface", "value": i})
                list_hst_hwdb.remove(maintenance_zabbix[item]['visiblename'])
                update.append(diction)
            elif (int(datetime.now().timestamp()) - maintenance_zabbix[item]['active_till']) > differentday:
                delet.append(str(maintenance_zabbix[item]['maintenanceid']))
        for i in list_hst_hwdb:
            diction = {
                "zbx_server": zabbix_servers[config.segment][zabbix]['selfportal_name'],
                "hostname": [i],
                "start_date": maintenance_hwdb[i]['startdate'],
                "period": maintenance_hwdb[i]['duration']
            }
            if len(maintenance_hwdb[i]['description']) > 6:
                diction['description'] = maintenance_hwdb[i]['description']
            else:
                diction['description'] = "Default description for automatic maintenance"
            if maintenance_hwdb[i]['interfaces'][0] != None:
                for t in maintenance_hwdb[i]['interfaces']:
                    if 'tags' not in diction:
                        diction['tags'] = []
                    if t != None:
                        diction['tags'].append({"tag": "interface", "value": t})
            add.append(diction)
        deleted.append({"zbx_server": zabbix_servers[config.segment][zabbix]['selfportal_name'], "maintenanceid": delet})
    return add, update, deleted

def start_work(zabbix):
    zabb = zabbix_servers[config.segment][zabbix]
    sql_answer = connect_pgsql(zabb['configur_pgsql'])
    if zabbix not in result_maria_maintenance:
        severity_log = "INFORMATION"
        message_log = "Not new maintenance in zabbix:%s" %(zabbix)
        loging_file(severity_log, message_log)
        add_maintenace, update_maintenace, delete_maintenance = sorted_hosts(None, sql_answer, zabbix)
    else:
        hosts_maintenance_hwdb = result_maria_maintenance[zabbix]
        add_maintenace, update_maintenace, delete_maintenance = sorted_hosts(hosts_maintenance_hwdb, sql_answer, zabbix)
    if len(add_maintenace) > 0:
        requests.post(config.url_api[config.segment], data=add_maintenace, headers=config.headers).json()
    if len(update_maintenace) > 0:
        requests.patch(config.url_api[config.segment], data=update_maintenace, headers=config.headers).json()
    if len(delete_maintenance[0]['maintenanceid']) > 0:
        requests.delete(config.url_api[config.segment], data=delete_maintenance, headers=config.headers).json()


if __name__ == '__main__':
    fp = open(os.path.realpath(__file__), 'r')
    try:
        fcntl.flock(fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError:
        name = sys.argv[0].split('/')[-1]
        severity_log = "INFORMATION"
        message_log = "Programm maintenance already exist"
        loging_file(severity_log, message_log)
        sys.exit(0)

    path_conf_zbx = config.path_dir + 'Zabbix.json'
    with open(path_conf_zbx, 'r') as serv:
        zabbix_servers = json.load(serv)
    path_conf_hwdb = config.path_dir + 'HWDB.json'
    with open(path_conf_hwdb, 'r') as maria:
        hardwaredb = json.load(maria)
    path_conf_mail = config.path_dir + 'Mails.json'
    with open(path_conf_mail, 'r') as mail:
        mails = json.load(mail)

    now = datetime.now()
    date = now.strftime("%d.%m.%Y %H:%M")
    differentday = config.days_after_del_maintenance * 24 * 60 *60

    lst_item = connect_maria(hardwaredb['configuration'][config.segment])
    result_maria_maintenance = {}
    for item in lst_item:
        item[3] = int(item[3].timestamp())
        duration = int(item[4].timestamp()) - item[3]
        if item[0] == None:
            item[0] = "NotZabb"
        else:
            item[0] = item[0].lower()
        if item[0] not in result_maria_maintenance:
            result_maria_maintenance[item[0]] = {}
        if item[2] not in result_maria_maintenance[item[0]]:
            result_maria_maintenance[item[0]][item[2]] = {}
            result_maria_maintenance[item[0]][item[2]] = {
                "description": item[1],
                "startdate": item[3],
                "duration": duration
            }
        if "interfaces" not in result_maria_maintenance[item[0]][item[2]]:
            result_maria_maintenance[item[0]][item[2]]["interfaces"] = []
        result_maria_maintenance[item[0]][item[2]]["interfaces"].append(item[5])

    print(result_maria_maintenance)

    lst_zabbix = []
    for zbx in zabbix_servers[config.segment]:
        lst_zabbix.append(zbx)

    with ThreadPool(len(lst_zabbix)) as p:
        p.map(start_work, lst_zabbix)

