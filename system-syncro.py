#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import sys
sys.path.append("venv/Lib/site-packages/")
import Configuration.default_config as config

import json
import fcntl
import os
import smtplib
import email
import email.mime.application
import re
import pymysql
from subprocess import Popen
from subprocess import PIPE
from pyzabbix import ZabbixAPI, ZabbixAPIException
from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def connect_maria(configure, mode_select, hostname):
    select_for_hosts = "SELECT hostname, INET_NTOA(mgmtip), zabbix FROM devices WHERE hostname=\""+ hostname + "\""
    select_for_lists = configure['SQL_Select_lst']

    connection_hardwaredb = pymysql.connect(host=configure['server'], user=configure['user'], password=configure['password'], database=configure['db'])
    cursor = connection_hardwaredb.cursor()
    if mode_select == 0:
        cursor.execute(select_for_lists)
    elif mode_select == 1:
        cursor.execute(select_for_hosts)
    result = cursor.fetchall()
    lists_item_db = []
    for rows in result:
        f = []
        for row in rows:
            f.append(row)
        lists_item_db.append(f)
    connection_hardwaredb.close()
    return lists_item_db

def connect_pgsql(config_pgsql):
    walkstring = "PGPASSWORD="+ config_pgsql['dbpassword'] +" psql -h "+ config_pgsql['dbhost'] +" -p "+ config_pgsql['dbport'] +" -U "+ config_pgsql['dbuser'] +" -w -d " + config_pgsql['dbread'] + " -c \"(" + config.sql_hst_zbx + ")\""
    child = Popen(walkstring, shell=True, stdout=PIPE)
    result = child.stdout.readlines()
    list_item_zbbx = []
    for x in result:
        x = x.decode("utf-8")
        x = x.replace("\n", "")
        x = x.replace(" ", "")
        lst = x.split("|")
        list_item_zbbx.append(lst)
    item = [0, 0, -1, -1]
    for x in item:
        list_item_zbbx.pop(x)
    return list_item_zbbx

def connect_api_zabbix(url_zab, user_zab, password_zab):
    zabbix_api = ZabbixAPI(url=url_zab, user=user_zab, password=password_zab)
    return zabbix_api

def add_hst_zbx(version_zbx, hostname, params, zabbix):
    try:
        severity_log = "INFORMATION"
        message_log = "Host:%s add to zabbix:%s" % (hostname, zabbix)
        add_def_group_id = ZabAPI.hostgroup.get(filter={'name': config.name_grp}, output=['groupid'])
        if params['virtual'] == None:
            try:
                try:
                    tmpl = zabbix_templates[params['model']][config.segment]['novirtual']
                except KeyError as err:
                    severity_log = "INFORMATION"
                    message_log = "Template:%s not in config Templates.json error:%s not add in zabbix:%s" % (params['model'], err, zabbix)
                    loging_file(severity_log, message_log)
                    return
                template_id = ZabAPI.template.get(filter={'host': tmpl}, output=['templateid'])
            except ZabbixAPIException as err:
                severity_log = "WARNING"
                message_log = "Template:%s not create in zabbix:%s with problem:%s" % (zabbix_templates[params['model']][config.segment]['novirtual'], zabbix, err)
                loging_file(severity_log, message_log)
                return
        else:
            try:
                try:
                    tmpl = zabbix_templates[params['model']][config.segment]['virtual']
                except KeyError as err:
                    severity_log = "INFORMATION"
                    message_log = "Template:%s not in config Templates.json error:%s" % (params['model'], err)
                    loging_file(severity_log, message_log)
                    return
                template_id = ZabAPI.template.get(filter={'host': tmpl}, output=['templateid'])
            except ZabbixAPIException as err:
                severity_log = "WARNING"
                message_log = "Template:%s not create in zabbix:%s with problem:%s" % (zabbix_templates[params['model']][config.segment]['virtual'], zabbix, err)
                loging_file(severity_log, message_log)
                return
        proxy_id = proxy_selection(params['vendor'])
        host_proxies = ZabAPI.host.get(hostids=proxy_id)
        proxy_id = host_proxies[0]['proxy_hostid']
        if version_zbx == '4':
            ZabAPI.host.create(host=hostname, proxy_hostid=proxy_id, inventory_mode=1, inventory={'date_hw_install':date}, groups=[{"groupid":add_def_group_id[0]['groupid']}], interfaces={'type':2, 'main':1, 'useip':1, 'ip':params['ip'], 'dns':'', 'port':'161'}, templates={'templateid':template_id[0]['templateid']})
        elif version_zbx == '5':
            if params['snmp'] == config.snmp2:
                ZabAPI.host.create(host=hostname, proxy_hostid=proxy_id, inventory_mode=1, inventory={'date_hw_install':date}, groups=[{"groupid":add_def_group_id[0]['groupid']}], interfaces={'type':2, 'main':1, 'useip':1, 'ip':params['ip'], 'details':{'version':2, 'community':snmps['snmpv2'][config.segment]['community']}, 'dns':'', 'port':'161'}, templates={'templateid':template_id[0]['templateid']})
            elif params['snmp'] in snmps['snmpv3'][config.segment]:
                ZabAPI.host.create(host=hostname, proxy_hostid=proxy_id, inventory_mode=1, inventory={'date_hw_install':date}, groups=[{"groupid":add_def_group_id[0]['groupid']}], interfaces={'type':2, 'main':1, 'useip':1, 'ip':params['ip'], 'details':snmps['snmpv3'][config.segment][params['snmp']], 'dns':'', 'port':'161'}, templates={'templateid':template_id[0]['templateid']})
            else:
                severity_log = "WARNING"
                message_log = "Host:%s have not correct attribute snmp" % (hostname)
    except ZabbixAPIException as err:
        severity_log = "WARNING"
        message_log = "Host:%s can not add to zabbix:%s with problem:%s" % (hostname, zabbix, err)
    loging_file(severity_log, message_log)

def update_hst_zbx(version_zbx, hostname, params, zabbix):
    try:
        severity_log = "INFORMATION"
        message_log = "Host:%s modify in zabbix:%s" % (hostname, zabbix)
        interf_id_get = ZabAPI.hostinterface.get(hostids=params['hostid'], output=['interfaceid'])
        if version_zbx == '4':
            ZabAPI.hostinterface.update(interfaceid=interf_id_get[0]['interfaceid'], ip=params['ip'])
        elif version_zbx == '5':
            if params['snmp'] == config.snmp2:
                ZabAPI.hostinterface.update(interfaceid=interf_id_get[0]['interfaceid'], ip=params['ip'], port=161,
                                            details={'version': 2, 'community':
                                                    snmps['snmpv2'][config.segment]['community']})
            elif params['snmp'] in snmps['snmpv3'][config.segment]:
                ZabAPI.hostinterface.update(interfaceid=interf_id_get[0]['interfaceid'], ip=params['ip'], port=161,
                                            details=snmps['snmpv3'][config.segment][params['snmp']])
            else:
                severity_log = "WARNING"
                message_log = "Host:%s have not correct attribute snmp" % (hostname)
    except ZabbixAPIException as err:
        severity_log = "PROBLEM"
        message_log = "Host:%s not modify in zabbix:%s with problem:%s" % (hostname, zabbix, err)
    loging_file(severity_log, message_log)

def del_hst_zbx(version_zbx, hostname, params, zabbix):
    try:
        severity_log = "INFORMATION"
        message_log = "Host:%s remove from zabbix:%s" % (hostname, zabbix)
        ZabAPI.host.delete(params['hostid'])
    except ZabbixAPIException as err:
        severity_log = "WARNING"
        message_log = "Host:%s can not delete from zabbix:%s with problem:%s" % (hostname, zabbix, err)
    loging_file(severity_log, message_log)

def add_group(url, user, password, hostname):
    ZabAPI = connect_api_zabbix(url, user, password)
    group_id = ZabAPI.hostgroup.get(filter={'name': 'На удаление'}, output=['groupid'])
    host_id = ZabAPI.host.get(filter={'host': hostname}, output=['hostid'])
    groups_hosts = ZabAPI.host.get(filter={'host': hostname}, output=['hostid'], selectGroups='extend')
    all_groups = []
    group_s = {}
    for i in groups_hosts[0]['groups']:
        group_s['groupid'] = i['groupid']
        all_groups.append(group_s.copy())
    group_s['groupid'] = group_id[0]['groupid']
    all_groups.append(group_s.copy())
    ZabAPI.host.update(hostid=host_id[0]['hostid'], groups=all_groups)
    ZabAPI.user.logout()

def mark_group(zbx_lst, zbx_con):
    name_1 = connect_maria(hardwaredb, 1, zbx_lst[1])
    name_2 = connect_maria(hardwaredb, 1, zbx_lst[5])
    if len(name_1) == 0:
        for i in zbx_con:
            if zbx_lst[0] == i['server']:
                add_group(i['configur_zabapi']['url'], i['configur_zabapi']['user'], i['configur_zabapi']['password'], zbx_lst[1])
    elif len(name_2) == 0:
        for i in zbx_con:
            if zbx_lst[4] == i['server']:
                add_group(i['configur_zabapi']['url'], i['configur_zabapi']['user'], i['configur_zabapi']['password'], zbx_lst[5])
    elif len(name_1) != 0 and len(name_2) != 0:
        if name_1[0][2] == name_2[0][2]:
            if name_1[0][2].upper() == zbx_lst[0].upper() and zbx_lst[0] != zbx_lst[4]:
                for i in zbx_con:
                    if zbx_lst[0] == i['Server']:
                        add_group(i['configur_zabapi']['url'], i['configur_zabapi']['user'],
                                  i['configur_zabapi']['password'], zbx_lst[1])
            elif name_1[0][2].upper() == zbx_lst[4].upper() and zbx_lst[0] != zbx_lst[4]:
                for i in zbx_con:
                    if zbx_lst[4] == i['Server']:
                        add_group(i['configur_zabapi']['url'], i['configur_zabapi']['user'],
                                  i['configur_zabapi']['password'], zbx_lst[5])

def find_min_val(lst_proxie):
    mini = min(d['value'] for d in lst_proxie if 'value' in d)
    for i in lst_proxie:
        if i['value'] == mini:
            hosted = i['hostid']
            break
    return hosted

def proxy_selection(name_vendor):
    lst_proxie_with_problem = []
    lst_proxie_without_problem = []
    if name_vendor == 'Cisco':
        group_id = ZabAPI.hostgroup.get(filter={'name':'Zabbix/Proxies/Cisco'}, output=['groupid'])
        if len(group_id) != 0:
            test = ZabAPI.host.get(groupids=group_id[0]['groupid'], output=['hostid'])
        if len(group_id) == 0 or len(test) == 0:
            group_id = ZabAPI.hostgroup.get(filter={'name':'Zabbix/Proxies/Other'}, output=['groupid'])
    elif name_vendor == 'Huawei':
        group_id = ZabAPI.hostgroup.get(filter={'name':'Zabbix/Proxies/Huawei'}, output=['groupid'])
        if len(group_id) != 0:
            test = ZabAPI.host.get(groupids=group_id[0]['groupid'], output=['hostid'])
        if len(group_id) == 0 or len(test) == 0:
            group_id = ZabAPI.hostgroup.get(filter={'name':'Zabbix/Proxies/Other'}, output=['groupid'])
    else:
        group_id = ZabAPI.hostgroup.get(filter={'name':'Zabbix/Proxies/Other'}, output=['groupid'])
    proxies = ZabAPI.host.get(groupids=group_id[0]['groupid'], output=['hostid'])
    for i in proxies:
        triggers = ZabAPI.trigger.get(hostids=i['hostid'], output=['value'], filter={'description':'Requires balancing of hosts'})
        if triggers[0]['value'] == '0':
            item_val =ZabAPI.item.get(hostids=i['hostid'], output=['lastvalue'], filter={'name':'Utilization of proxy for balansing'})
            param = {'value':item_val[0]['lastvalue'], 'hostid':i['hostid']}
            lst_proxie_without_problem.append(param)
        elif triggers[0]['value'] == '1':
            item_val =ZabAPI.item.get(hostids=i['hostid'], output=['lastvalue'], filter={'name':'Utilization of proxy for balansing'})
            param = {'value':item_val[0]['lastvalue'], 'hostid':i['hostid']}
            lst_proxie_with_problem.append(param)
    if len(lst_proxie_without_problem) != 0:
        hostid = find_min_val(lst_proxie_without_problem)
    else:
        hostid = find_min_val(lst_proxie_with_problem)
    return hostid

def loging_file(severity, message):
    date = datetime.now().strftime("%Y%m%d")
    time = datetime.now().strftime("%H:%M")
    logfile_name = str(date) + "-SHWDB.log"
    logfile = config.dir_log + "/" + logfile_name
    record = severity + "|" + str(time) + "|" + message + "\n"
    write_log = open(logfile, "a")
    write_log.write(record)
    write_log.close()

def report_file(lst_hosts):
    file_name = "report syncro " + config.segment + " " + date + ".csv"
    work_f = open(file_name, "w")
    work_f.write('Hostname' + ";" + 'Zabbix' + ";" + 'type' + ";" + 'ip' + "\n")
    for element in lst_hosts:
        result = map(lambda x: f'="{x}"', element)
        work_f.write(';'.join(result)+"\n")
    work_f.close()
    return file_name

def mail_send(configuration_serv, person, file):
    msg = MIMEMultipart()
    msg['Subject'] = 'Report syncronization from HardwareDB to Zabbix'
    msg['From'] = configuration_serv["mail_from"]
    body = MIMEText("""
                    This email is generated by an automated system.
                    Please do not reply.

                    not_work_device = Device does not work
                    not_zbx_in_sql = Attribute zabbix is not defined
                    not_host_in_db = Hostname does not found in the DB

                    not_work_snmp_and_icmp
                    not_work_snmp
                    not_work_icmp
                    """)
    msg.attach(body)
    mail_to = person["Mail"]
    filename = file
    fp = open(filename, 'rb')
    att = email.mime.application.MIMEApplication(fp.read(), _subtype="zip")
    fp.close()
    att.add_header('Content-Disposition', 'attachment', filename=filename)
    msg.attach(att)
    server = smtplib.SMTP(configuration_serv["server"], port=configuration_serv["port"])
    server.ehlo(configuration_serv["ehlo"])
    server.sendmail(msg['From'], mail_to, msg.as_string())
    server.quit()


if __name__ == '__main__':
    fp = open(os.path.realpath(__file__), 'r')
    try:
        fcntl.flock(fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError:
        name = sys.argv[0].split('/')[-1]
        severity_log = "INFORMATION"
        message_log = "Programm already exist"
        loging_file(severity_log, message_log)
        sys.exit(0)

    path_conf_zbx = config.path_dir + 'Zabbix.json'
    with open(path_conf_zbx, 'r') as serv:
        zabbix_servers = json.load(serv)
    path_conf_tmpl = config.path_dir + 'Templates.json'
    with open(path_conf_tmpl, 'r') as template:
        zabbix_templates = json.load(template)
    path_conf_hwdb = config.path_dir + 'HWDB.json'
    with open(path_conf_hwdb, 'r') as maria:
        hardwaredb = json.load(maria)
    path_conf_mail = config.path_dir + 'Mails.json'
    with open(path_conf_mail, 'r') as mail:
        mails = json.load(mail)
    path_conf_snmp = config.path_dir + 'SNMP-VER.json'
    with open(path_conf_snmp, 'r') as snmp:
        snmps = json.load(snmp)

    now = datetime.now()
    date = now.strftime("%d.%m.%Y %H:%M")

    lst_item = connect_maria(hardwaredb['Configuration'][config.segment], 0, "None")
    result_maria = {}
    for item in lst_item:
        if item[8].lower() not in result_maria:
            result_maria[item[8].lower()] = {}
        if item[7] == '':
            item[7] = None
        if item[5] == '':
            item[5] = None
        result_maria[item[8].lower()][item[0]] = {
            'ip': item[1],
            'ip_ext': item[2],
            'segment': item[3],
            'vendor': item[4],
            'model': item[5],
            'dcname': item[6],
            'virtual': item[7],
            'snmp': item[9],
            }

    for key_zab in zabbix_servers[config.segment]:
        zabb = zabbix_servers[config.segment][key_zab]
        lst_hst_zbx = connect_pgsql(zabb['configur_pgsql'])
        diction_hst_zbx = {}
        for item in lst_hst_zbx:
            if item[1] not in diction_hst_zbx:
                diction_hst_zbx[item[1]] = {}
            if item[4] == '2':
                snmps_ver = config.snmp2
            elif item[4] == '3':
                for name_snmp in snmps['snmpv3'][config.segment]:
                    if item[5] == snmps['snmpv3'][config.segment][name_snmp]['securityname']:
                        snmps_ver = name_snmp
                        break
                    else:
                        snmps_ver = config.snmp3
            diction_hst_zbx[item[1]] = {
                'hostid': item[0],
                'ip': item[3],
                'snmp': snmps_ver
                }

        hst_add_to_zbx = {}
        hst_mod_in_zbx = {}
        hst_another = {}
        try:
            for hst in result_maria[key_zab]:
                try:
                    zbx_hst = diction_hst_zbx[hst]
                    maria_hst = result_maria[key_zab][hst]
                    if maria_hst['ip'] == zbx_hst['ip'] and maria_hst['snmp'] == zbx_hst['snmp']:
                        del diction_hst_zbx[hst]
                    elif maria_hst['ip'] != zbx_hst['ip'] or maria_hst['snmp'] != zbx_hst['snmp']:
                        if hst not in hst_mod_in_zbx:
                            hst_mod_in_zbx[hst] = {}
                        hst_mod_in_zbx[hst] = {
                            'hostid': zbx_hst['hostid'],
                            'ip': maria_hst['ip'],
                            'model': maria_hst['model'],
                            'virtual': maria_hst['virtual'],
                            'snmp': maria_hst['snmp']
                            }
                        del diction_hst_zbx[hst]
                    else:
                        if hst not in hst_another:
                            hst_another[hst] = {}
                        hst_another[hst] = maria_hst
                except KeyError as error_hst:
                    if hst not in hst_add_to_zbx:
                        hst_add_to_zbx[hst] = {}
                    hst_add_to_zbx[hst] = {
                        'ip': result_maria[key_zab][hst]['ip'],
                        'model': result_maria[key_zab][hst]['model'],
                        'virtual': result_maria[key_zab][hst]['virtual'],
                        'vendor': result_maria[key_zab][hst]['vendor'],
                        'snmp': result_maria[key_zab][hst]['snmp']
                        }
        except KeyError as error_zabb:
            severity_log = 'INFORMATION'
            message_log = "In HW not result for zabbix:%s" % (error_zabb)
            loging_file(severity_log, message_log)

        result_maria[key_zab] = hst_another
        hst_del_zbx = diction_hst_zbx

        if result_maria[key_zab]:
            severity_log = "INFORMATION"
            message_log = "Not all list HWDB processed for zabbix:%s" % (key_zab)
            loging_file(severity_log, message_log)

        try:
            ZabAPI = connect_api_zabbix(zabb['configur_zabapi']['url'], zabb['configur_zabapi']['user'], zabb['configur_zabapi']['password'])
        except ZabbixAPIException as e:
            severity_log = "WARNING"
            message_log = e
            loging_file(severity_log, message_log)
            break
        version_zabbix = ZabAPI.apiinfo.version()
        version_zabbix = re.match('^[0-9]', version_zabbix)
        version_zabbix = version_zabbix.group()

        count_repeat_update = 0
        if version_zabbix=='4' or version_zabbix=='5':
            if hst_add_to_zbx:
                for hst_add in hst_add_to_zbx:
                    if hst_add_to_zbx[hst_add]['model'] == None:
                        severity_log = "PROBLEM"
                        message_log = "In HWDB host:%s have not model" % (hst_add)
                        loging_file(severity_log, message_log)
                    else:
                        add_hst_zbx(version_zabbix, hst_add, hst_add_to_zbx[hst_add], key_zab)
            else:
                severity_log = "INFORMATION"
                message_log = "List add empty for zabbix:%s" % (key_zab)
                loging_file(severity_log, message_log)
            if hst_mod_in_zbx:
                for upd_hst in hst_mod_in_zbx:
                    count_repeat_update = count_repeat_update + 1
                    if count_repeat_update <= config.limit_update:
                        update_hst_zbx(version_zabbix, upd_hst, hst_mod_in_zbx[upd_hst], key_zab)
                    else:
                        break
            else:
                severity_log = "INFORMATION"
                message_log = "List update empty for zabbix:%s" % (key_zab)
                loging_file(severity_log, message_log)
            if hst_del_zbx:
                for del_hst in hst_del_zbx:
                    del_hst_zbx(version_zabbix, del_hst, hst_del_zbx[del_hst], key_zab)
            else:
                severity_log = "INFORMATION"
                message_log = "List del empty for zabbix:%s" % (key_zab)
                loging_file(severity_log, message_log)
        else:
            severity_log = "WARNING"
            message_log = "Incorrect detect version:%s in zabbix:%s" % (version_zabbix, key_zab)
            loging_file(severity_log, message_log)

        ZabAPI.user.logout()