# ======================================== Block general propertys =====================================================
# Limits
limit_update = 5

# Segments start collect
# Valid value EXT or INT
segment = 'EXT'

# Main dirictory
dir_hwdb = '/opt/hardwaredb/'

# LOG dir
dir_log = dir_hwdb + 'Log'

# Path for Configuration system HWDB Syncro
path_dir = dir_hwdb + 'Configuration/'

# ======================================== Block HWDB syncro hosts =====================================================
# SQL for Zabbix DB
sql_hst_zbx = "SELECT h.hostid, h.host, h.name, i.ip, isn.version, isn.securityname FROM hosts h LEFT JOIN interface i USING (hostid)\
               LEFT JOIN hosts_groups USING (hostid) LEFT JOIN hstgrp hs USING (groupid) LEFT JOIN interface_snmp isn USING (interfaceid)\
               WHERE i.ip <> '' AND i.type=2 except SELECT h.hostid, h.host, h.name, i.ip, isn.version, isn.securityname FROM hosts h\
               LEFT JOIN interface i USING (hostid) LEFT JOIN hosts_groups USING (hostid) LEFT JOIN hstgrp hs USING (groupid)\
               LEFT JOIN interface_snmp isn USING (interfaceid) WHERE i.ip <> '' AND i.type=2 AND hs.name='На удаление'\
               GROUP BY h.hostid, h.host, h.name, i.ip, isn.version, isn.securityname"

# SNMP translate
snmp2 = 'V2' + segment
snmp3 = 'V3CIS'

# Default name group for host add
name_grp = 'DC/From_inventory'


# ======================================== Block HWDB syncro maintenance ===============================================
# SQL for maintenance
sql_maintenance_zbx = f"SELECT a.maintenanceid, a.name as maintenancename, a.description, a.active_since, a.active_till, t.tag, t.operator, t.value," \
                      f"h.hostid, h.host as hostname, h.status, h.name as visiblename, g.name as groupname, g.groupid FROM maintenances a left join maintenance_tag t " \
                      f"on a.maintenanceid = t.maintenanceid left join maintenances_windows w on a.maintenanceid = w.maintenanceid " \
                      f"left join timeperiods ti on w.timeperiodid = ti.timeperiodid left join maintenances_hosts mh " \
                      f"on a.maintenanceid = mh.maintenanceid left join hosts h on a.maintenanceid = h.maintenanceid " \
                      f"left join maintenances_groups mg on a.maintenanceid = mg.maintenanceid left join hstgrp g " \
                      f"on mg.groupid = g.groupid"

# URL selfportal api packets maintenance
url_api = {
    "INT": "http://selfportal.zabbix.ca.sbrf.ru/api/maintenance/batch",
    "EXT": "http://selfportal.zabbix.sigma.sbrf.ru/api/maintenance/batch"
}

# Auth token selfportal
token_api = ""

headers = {
    "Authorization": "Bearer %s" % (token_api),
    "Content-Type": "application/json"
}

# Days before expiration maintenance
days_after_del_maintenance = 90